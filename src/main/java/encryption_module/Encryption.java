package encryption_module;

import com.grayen.encryption.caesar.algorithm.Caesar;
import com.grayen.encryption.caesar.algorithm.implementation.CaesarFabric;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class Encryption {

    private static final int LEFT_SHIFT_ENCRYPTION = 3;

    public static void main(String[] args) {

        String securityQuestion = getSecurityQuestionFromInput();
        String encryptedQuestion = encryptSecurityQuestion(securityQuestion);

        try {
            saveToDb(encryptedQuestion);
            System.out.println("Success!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static String encryptSecurityQuestion(String securityQuestion) {
        Caesar encryption = CaesarFabric.getEncryptionSystem();

        String encryptedText = encryption.encrypt(securityQuestion, LEFT_SHIFT_ENCRYPTION);
/*        System.out.println("DEBUG: encryptedText = " + encryptedText);

        String decryptedText = encryption.decrypt(encryptedText, LEFT_SHIFT_ENCRYPTION);
        System.out.println("DEBUG: decryptedText = " + decryptedText);
*/
        return encryptedText;
    }

    private static String getSecurityQuestionFromInput() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter your Security Question");
        String securityQuestion = sc.nextLine();

        if (securityQuestion.length() > 100) {
            throw new IllegalArgumentException("Question size was over 100");
        }

        return securityQuestion;
    }

    private static void saveToDb(String encryptedQuestion) throws SQLException {
        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "1234")) {
            String query = "UPDATE customer "
                    + " SET encryptedSecurityQuestion = ?"
                    + " WHERE customerid = 12341";

            PreparedStatement prepStatement = connection.prepareStatement(query);
            prepStatement.setString(1, encryptedQuestion);

            prepStatement.executeUpdate();
        }

    }

}
