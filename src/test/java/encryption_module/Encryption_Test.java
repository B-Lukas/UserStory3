package encryption_module;

import org.junit.Assert;
import org.junit.Test;

import static encryption_module.Encryption.encryptSecurityQuestion;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertEquals;

public class Encryption_Test {

    String securityQuestion = "abc?";

    @Test
    public void passEncryption() {
        assertEquals("xyz?", encryptSecurityQuestion(securityQuestion));
    }

    @Test
    public void failEncryption() {
        Assert.assertThat("yxcycxyc", not(equalTo(encryptSecurityQuestion(securityQuestion))));
    }
}
